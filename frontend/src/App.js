import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import DeleteIcon from '@material-ui/icons/Delete';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListIcon from '@material-ui/icons/List';
import AddIcon from '@material-ui/icons/Add';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import produce from 'immer';

export class App extends Component {
    state = {
        drawer_open: false,
        lists: [
            {'id': 0, 'name': 'School List', 'tasks': [{'id': 0, 'text': 'Weekly coding challenge', 'complete': false}, {'id': 1, 'text': 'Attend office hours', 'complete': false}]},
            {'id': 1, 'name': 'Work List', 'tasks': [{'id': 2, 'text': 'Build pricing model', 'complete': false}, {'id': 3, 'text': 'Schedule staff meeting', 'complete': false}]},
            {'id': 2, 'name': 'Shopping List', 'tasks': [{'id': 4, 'text': 'Cheerios', 'complete': false}, {'id': 5, 'text': 'Apples', 'complete': false}]}
        ],
        list_id: 0,
        new_task: "",
        new_list: "",
    };

    handleAddTask = () => {
        if (this.state.new_task !== "") {
            const list_index = this.state.lists.findIndex(list => list.id === this.state.list_id);
            const new_id = Math.floor(Math.random()*90000) + 10000;

            this.setState({
                lists: produce(this.state.lists, (draft) => {draft[list_index].tasks.push({'id': new_id, 'text': this.state.new_task, 'complete': false})}),
                new_task: ""
            });
        }
    };

    handleDeleteTask = (task_id) => {
        const list_index = this.state.lists.findIndex(list => list.id === this.state.list_id);
        const task_index = this.state.lists[list_index].tasks.findIndex(task => task.id === task_id);

        this.setState({
            lists: produce(this.state.lists, (draft) => {draft[list_index].tasks.splice(task_index, 1)})
        });
    };

    handleCheck = (task_id, checked) => {
        const list_index = this.state.lists.findIndex(list => list.id === this.state.list_id);
        const list = this.state.lists[list_index];
        const task_index = list.tasks.findIndex(task => task.id === task_id);
        this.setState({
            lists: produce(this.state.lists, (draft) => {draft[list_index].tasks[task_index].complete = checked})
        })
    };

    handleAddList = () => {
        if (this.state.new_list !== "") {
            const new_id = Math.floor(Math.random()*90000) + 10000;
            this.setState({
                lists: produce(this.state.lists, (draft) => {draft.push({'id': new_id, 'name': this.state.new_list, 'tasks': []})}),
                new_list: "",
                add_list_open: false
            });
        }
    };

    handleDeleteList = () => {
        const list_index = this.state.lists.findIndex(list => list.id === this.state.list_id);
        this.setState({
            lists: produce(this.state.lists, (draft) => {draft.splice(list_index, 1)}),
            list_id: null
        });
    };

    render() {
        return (
            <div style={{flexGrow: 1}}>
                <CssBaseline />

                {/* The AppBar */}
                <AppBar position="static">
                    <Toolbar>
                        <IconButton style={{marginLeft: -12, marginRight: 20}} color="inherit" onClick={() => this.setState({drawer_open: true})}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" style={{flexGrow: 1}}>
                            {this.state.list_id !== null ? this.state.lists.find(list => list.id === this.state.list_id).name : "To Do List"}
                        </Typography>
                        <Button color="inherit">Sign Out</Button>
                    </Toolbar>
                </AppBar>

                {/* The Drawer */}
                <Drawer open={this.state.drawer_open} onClose={() => this.setState({drawer_open: false})}>
                    <div>
                        {this.state.lists.map(list => {
                            return (
                                <ListItem key={list.id} button onClick={() => this.setState({list_id: list.id, drawer_open: false})}>
                                    <ListItemIcon>
                                        <ListIcon />
                                    </ListItemIcon>
                                    <ListItemText primary={list.name} />
                                </ListItem>
                            )
                        })}
                        <ListItem button onClick={() => this.setState({add_list_open: true})}>
                            <ListItemIcon>
                                <AddIcon />
                            </ListItemIcon>
                            <ListItemText primary="Create List" />
                        </ListItem>
                    </div>
                </Drawer>

                {/* The List */}
                {this.state.list_id  !== null && <div style={{display: 'flex', justifyContent: 'center'}}>
                    <Paper style={{maxWidth: '1020px', width: '100%', marginTop: '40px'}}>
                        <div>
                            <div style={{display: 'flex', padding: '24px 24px'}}>
                                <Typography variant="headline" style={{flexGrow: 1}}>{this.state.lists.find(list => list.id === this.state.list_id).name}</Typography>
                                <Button color="secondary" variant="raised" size="small" onClick={this.handleDeleteList}>Delete List</Button>
                            </div>
                            <ListItem>
                                <ListItemText primary={<TextField autoFocus placeholder="Type a new task" style={{width: '100%'}} value={this.state.new_task} onChange={(e) => this.setState({new_task: e.target.value})}/>} />
                                <ListItemIcon><IconButton onClick={this.handleAddTask}><AddIcon/></IconButton></ListItemIcon>
                            </ListItem>
                            {this.state.lists.find(list => list.id === this.state.list_id).tasks.map(task => {
                                return (
                                    <ListItem key={task.id}>
                                        <ListItemIcon>
                                            <Checkbox
                                                checked={task.complete}
                                                onChange={(e, checked) => this.handleCheck(task.id, checked)}
                                            />
                                        </ListItemIcon>
                                        <ListItemText primary={task.text}/>
                                        <IconButton onClick={() => this.handleDeleteTask(task.id)}><DeleteIcon/></IconButton>
                                    </ListItem>
                                )
                            })}
                        </div>
                    </Paper>
                </div>}

                {/* The Add List Dialog */}
                <Dialog open={this.state.add_list_open} onClose={() => this.setState({add_list_open: false})}>
                    <DialogTitle>Create a new list</DialogTitle>
                    <DialogContent>
                        <ListItem>
                            <ListItemText primary={<TextField autoFocus placeholder="Name your list" style={{width: '100%'}} value={this.state.new_list} onChange={(e) => this.setState({new_list: e.target.value})} />} />
                            <ListItemIcon>
                                <IconButton onClick={this.handleAddList}><AddIcon/></IconButton>
                            </ListItemIcon>
                        </ListItem>
                    </DialogContent>
                </Dialog>
            </div>
        );
    }
}